Fortran HDF5 utilities
======================

This repository contains utilities for reading and writing to HDF5 in Fortran.

Build
-----

Compile the library, replacing `HDF5DIR` as appropriate for your installation of HDF5:

```
make HDF5DIR=/usr/local/opt/hdf5 build
```

Example
-------

Test using it:

```
cd example
make test
```

It should write an HDF5 file `test.h5` and run a Python program which prints `1234.0`.

*Note: the Python script requires h5py.*

Usage
-----

To use in your code, first build the library. Then include `hdf5_utils.mod` when compiling your code

```
-I${DIR_WITH_MOD_FILE}
```

and link to `libhdf5_utils.so` when building the executable

```
-L${DIR_WITH_SO_FILE} -lhdf5_utils
```

You can follow the example Makefile in the `example` directory.

See https://github.com/dmentipl/fortran-hdf5-utilities-as-submodule for an example of how to use this library as a git submodule.
