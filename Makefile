# ---------------------------------------------------------------------------- #
#
# Makefile for Fortran utilities for reading and writing HDF5 files
#
# To use the Fortran HDF5 library you need:
#   - the Fortran (compiler-dependent) .mod files
#   - the shared object libraries as .so files
#
# The .mod files are found in an include directory whereas the .so
# files are found in a lib directory. Often both directories are under
# one root, e.g. HDF5DIR= /usr/local/opt/hdf5. In this case just set
# HDF5DIR for your machine.
#
# However, sometimes these directories are separate. Then you must set
# both HDF5INC and HDF5LIB.
#
# Daniel Mentiplay, 2019.
#
# ---------------------------------------------------------------------------- #

# Operating system
UNAME = ${shell uname}

# Build and source dirs
BUILDDIR = build
SOURCEDIR = src

# Fortran compiler
FC = gfortran

# Compiler and linker flags
FCFLAGS = -O3 -fPIC
FLFLAGS = -shared

# Set HDF5 include and library directories
ifneq (X$(HDF5DIR), X)
    ifeq (X$(HDF5INC), X)
        HDF5INC= $(HDF5DIR)/include
    endif
    ifeq (X$(HDF5LIB), X)
        HDF5LIB= $(HDF5DIR)/lib
    endif
endif
ifeq (X$(HDF5INC), X)
	HDF5INC=
endif
ifeq (X$(HDF5LIB), X)
	HDF5LIB=
endif

# Add HDF5 to compiler and linker flags
FCFLAGS+= -I$(HDF5INC) -J./$(BUILDDIR)
FLFLAGS+= -L$(HDF5LIB) -lhdf5 -lhdf5_fortran

# Source and object files
FSOURCES = $(wildcard $(SOURCEDIR)/*.f90)
OBJECTS = $(patsubst $(SOURCEDIR)/%.f90,$(BUILDDIR)/%.o,$(FSOURCES))

# Rule to compile Fortran source
$(OBJECTS) : $(FSOURCES)
	$(FC) -c $(FCFLAGS) $< -o $@

.PHONY: help
help:
	@echo
	@echo " Fortran utilities for reading and writing HDF5 files"
	@echo
	@echo " Usage:"
	@echo "  make <target>"
	@echo
	@echo "  build        Build into shared object library"
	@echo "  clean        Clean up build files"
	@echo "  help         Display this help"
	@echo
	@echo " Variables:"
	@echo
	@echo "  HDF5DIR      HDF5 root install directory"
	@echo "  HDF5INC      HDF5 include directory [only set if not at \$$HDF5DIR/include]"
	@echo "  HDF5LIB      HDF5 library directory [only set if not at \$$HDF5DIR/lib]"
	@echo "  FC           Fortran compiler [optional (default: gfortran)]"
	@echo
	@echo " To use the Fortran HDF5 library you need:"
	@echo "   - the Fortran (compiler-dependent) .mod files at \$$HDF5INC"
	@echo "   - the shared object libraries as .so files at \$$HDF5LIB"
	@echo
	@echo " Usually both sets of files are in directories under one root,"
	@echo " e.g. HDF5INC=/usr/local/hdf5/include and HDF5LIB=/usr/local/hdf5/lib."
	@echo " In this case, set HDF5DIR=/usr/local/hdf5."
	@echo
	@echo " However, sometimes these directories are separate. Then you must set"
	@echo " both HDF5INC and HDF5LIB."
	@echo

$(BUILDDIR)/libhdf5_utils.so: check $(OBJECTS)
	@echo
	@echo " Linking HDF5 utils into a shared object library"
	@echo
	$(FC) $(FCFLAGS) $(FLFLAGS) $(OBJECTS) -o $@
ifeq ($(UNAME), Darwin)
	install_name_tool -id @rpath/libhdf5_utils.so $@
endif

.PHONY: build
build: $(BUILDDIR)/libhdf5_utils.so

.PHONY: clean
clean:
	@echo
	@echo " Removing Fortran build files"
	@echo
	@rm -rf build/*.o build/*.mod build/*.so build/*.dSYM

.PHONY: check
check:
	@! [ -z $(HDF5INC) ] || { echo "ERROR: HDF5INC not set"; exit 1; }
	@! [ -z $(HDF5LIB) ] || { echo "ERROR: HDF5LIB not set"; exit 1; }
	@test -d $(HDF5INC) || { echo "ERROR: HDF5INC=$(HDF5INC) does not exist"; exit 1; }
	@test -d $(HDF5LIB) || { echo "ERROR: HDF5LIB=$(HDF5LIB) does not exist"; exit 1; }
