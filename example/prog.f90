PROGRAM prog
  USE hdf5_utils, ONLY: write_to_hdf5,    &
                        create_hdf5file,  &
                        close_hdf5file,   &
                        create_hdf5group, &
                        close_hdf5group,  &
                        HID_T

  IMPLICIT NONE

  CHARACTER(len=100) :: file_name
  CHARACTER(len=100) :: group_name
  CHARACTER(len=100) :: dataset_name

  REAL               :: real_value

  INTEGER            :: error

  INTEGER(HID_T)     :: file_id
  INTEGER(HID_T)     :: group_id

  error        = 0
  file_name    = 'test.h5'
  group_name   = 'group'
  dataset_name = 'dset'
  real_value   = 1234.

  CALL create_hdf5file(file_name, file_id, error)
  IF (error /= 0) THEN
    WRITE(*,'("cannot create HDF5 file",/)')
    RETURN
  ENDIF

  CALL create_hdf5group(file_id, group_name, group_id, error)
  IF (error /= 0) THEN
    WRITE(*,'("cannot create HDF5 group",/)')
    RETURN
  ENDIF

  CALL write_to_hdf5(real_value, 'value', group_id, error)
  IF (error /= 0) THEN
    WRITE(*,'("cannot write data to HDF5 file",/)')
    RETURN
  ENDIF

  CALL close_hdf5group(group_id, error)
  IF (error /= 0) THEN
    WRITE(*,'("cannot close HDF5 group",/)')
    RETURN
  ENDIF

  CALL close_hdf5file(file_id, error)
  IF (error /= 0) THEN
    WRITE(*,'("cannot close HDF5 file",/)')
    RETURN
  ENDIF

END PROGRAM prog
